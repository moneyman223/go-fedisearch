package main

import (
	"github.com/microcosm-cc/bluemonday" // Strip HTML
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"io"
	"strings"
	"net/http"
	"os"
	"time"
	"net"
	"crypto/sha1"
	"github.com/jackc/pgx" // Postgres
	"context" // Also Postgres
	"encoding/binary"
)

type CommandType int8

const (
	SHUTDOWN	CommandType = 0
	STATUS		CommandType = 1
	ADDINSTANCE	CommandType = 2
	SUSPENDINSTANCE	CommandType = 3
	RESUMEINSTANCE	CommandType = 4
	SETCRAWL	CommandType = 5
)

type Settings struct {
	crawl		bool
	retrievers	[]RetrieverState
}

type State int8

const (
	ACTIVE		State = 0
	SUSPENDED	State = 1
	AUTHFAILURE	State = 2
	ERROROTHER	State = 3
)

type RetrieverState struct {
	hostname	string
	credentialed	bool
	state		State

	channel		chan string
}

type Post struct {
	Id			string `json:"id"`
	Url			string `json:"url"`

	Account			AccountType

	Content			string `json:"content"`

	// Custom Fields
	StrippedContent		string
	Posthash		[]byte
}

type AccountType struct {
	Acct			string	`json:"acct"`
	Avatar			string	`json:"avatar"`
	Bot			bool	`json:"bot"`
	Created_at		string	`json:"created_at"`
	Display_name		string	`json:"display_name"`
	Url			string	`json:"url"`
}

type RetrieverData struct {
	hostname		string
	payload			string // Place Holder
}

func dbwriter(post_channel chan Post) {
	conn, err := pgx.Connect(context.Background(), "postgres://postgres@127.0.0.1/tutorial")
	sqlSelectNewAcct := `SELECT id FROM accounts WHERE acct = $1`
	sqlInsertNewAcct := `INSERT INTO accounts (acct, avatar, bot, created_at, display_name, url) VALUES ($1, $2, $3, $4, $5, $6)`
	sqlCheckPostHash := `SELECT id FROM posts WHERE posthash = $1`
	sqlInsertNewPost :=`INSERT INTO posts (url, content, strippedcontent, posthash) VALUES ($1, $2, $3, $4)`

	if err != nil {
		fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	defer conn.Close(context.Background())

	for {
		newpost, ok := <-post_channel
		if !ok {
			fmt.Println("Break!")
			break
		}
		tx, err := conn.Begin(context.Background())

		// Check Accounts

		postrows, err := tx.Query(context.Background(), sqlCheckPostHash, newpost.Posthash)
		if err != nil {
			fmt.Println("First query panic")
			panic(err)
		}
		postcount := 0
		for postrows.Next() {
			postcount += 1
			break
		}
		postrows.Close()

		if postcount == 0 {
			_, err = tx.Exec(context.Background(), sqlInsertNewPost, newpost.Url, newpost.Content, newpost.StrippedContent, newpost.Posthash)
			if err != nil {
				fmt.Println("First execution")
				panic(err)
			}
			acctrows, err := tx.Query(context.Background(), sqlSelectNewAcct, newpost.Account.Acct)
			if err != nil {
				fmt.Println("Query panic")
				panic(err)
			}

			rowcount := 0
			for acctrows.Next() {
				rowcount += 1
				break
			}
			acctrows.Close()

			if rowcount == 0 {
				_, err = tx.Exec(context.Background(), sqlInsertNewAcct, newpost.Account.Acct,
						 newpost.Account.Avatar, newpost.Account.Bot, newpost.Account.Created_at,
						 newpost.Account.Display_name, newpost.Account.Url)
				if err != nil {
					fmt.Println("Exec panic")
					panic(err)
				}

				if err != nil {
					fmt.Println("Commit panic")
					panic(err)
				}

			}
		}

		err = tx.Commit(context.Background())
		tx.Rollback(context.Background())
	}
}

func getAccountKeys(api_base_url string) {

	requestBodymap, err := json.Marshal(map[string]string{
		"client_name":   "pytooterapp",
		"scopes":        "read write follow push",
		"redirect_uris": "urn:ietf:wg:oauth:2.0:oob",
	})
	requestBodybytes := bytes.NewBuffer(requestBodymap)

	api_base_api := api_base_url + "/api/v1/apps"

	fmt.Println("Connecting to " + api_base_api + "...")

	resp, err := http.Post(api_base_api, "application/json", requestBodybytes)
	if err != nil {
		fmt.Println("Error occured in building the client")
		os.Exit(1)
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	bodymsg := string(body)
	bodymap := make(map[string]interface{})

	err = json.Unmarshal([]byte(bodymsg), &bodymap)

	if err != nil {
		panic(err)
	}

	client_id := bodymap["client_id"]
	client_secret := bodymap["client_secret"]
	fmt.Println(client_id)
	fmt.Println(client_secret)
}

func authenticate(api_base_url string, user string, pass string) {

}

func peerhunter(api_base_url string, newpeer_chan chan string) {
	api_peers := "https://" + api_base_url + "/api/v1/instance/peers"
	resp, err := http.Get(api_peers)
	if err != nil {
		fmt.Println("Peer Instances failure...")
		os.Exit(1)
	}

	body, err := ioutil.ReadAll(resp.Body)

	var newpeers []string
	err = json.Unmarshal( []byte(body)  , &newpeers)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}

	for _, newpeer := range newpeers {
		newpeer_chan <- newpeer
	}
}

///////// Public Timeline
func publictimeline_retriever(api_base_url string, post_channel chan Post, state chan string, retrieverdata_chan chan RetrieverData) {
	p := bluemonday.NewPolicy()
	min_id := ""

	fmt.Println("Public timeline...")

	var retrieverdata RetrieverData

	retrieverdata.hostname = api_base_url

	for {

		api_timeline := "https://" + api_base_url + "/api/v1/timelines/public?min_id=" + min_id
		resp, err := http.Get(api_timeline)
		if err != nil {
			fmt.Println("Error occured in timeline...")
			os.Exit(1)
		}

		body, err := ioutil.ReadAll(resp.Body)

		newposts := make([]Post, 0)
		err = json.Unmarshal(body, &newposts)
		if err != nil {
			log.Fatal(err)
			panic(err)
		}

		// Determine minimum ID and send to database writer
		for _, newpost := range newposts {
			posthash := sha1.New()

			if strings.Contains(newpost.Account.Acct, "@") == false {
				newpost.Account.Acct += "@" + api_base_url
			}

			fmt.Fprint(posthash, newpost.Url)
			fmt.Fprint(posthash, newpost.Content)
			fmt.Fprint(posthash, newpost.Account.Acct)
			fmt.Fprint(posthash, newpost.Account.Created_at)
			fmt.Fprint(posthash, newpost.Account.Display_name)
			fmt.Fprint(posthash, newpost.Account.Url)
			newpost.Posthash = posthash.Sum(nil)

			newpost.StrippedContent = p.Sanitize(newpost.Content)

			if newpost.Id > min_id {
				min_id = newpost.Id
			}
			post_channel <- newpost

			//retrieverdata.payload = "ping"
		}
		resp.Body.Close()

		time.Sleep(time.Second * 2)
	}
}

/*
 * Going forward, this needs to simply return what will happen, not do it
*/
func parseCtl(c net.Conn, post_channel chan Post, settings *Settings, retrieverdata_chan chan RetrieverData) {

	rawCmd := make([]byte, 1)
	_, err := io.ReadFull(c, rawCmd)
	if err != nil {
		fmt.Println(err)
		c.Close()
	}

	// Read the flags
	rawSendFlags := make([]byte, 1)
	_, err = io.ReadFull(c, rawSendFlags)
	if err != nil {
		fmt.Println(err)
		c.Close()
	}

	switch CommandType(rawCmd[0]) {
	case SHUTDOWN:
		fmt.Println("Shutdown command received, unimplemented")
	case STATUS:
		var i uint32
		i = uint32(len(settings.retrievers))
		sizebyte := make([]byte, 4)
		binary.LittleEndian.PutUint32(sizebyte, uint32(i))
		fmt.Println(sizebyte)
		_, _ = c.Write(sizebyte)

		fmt.Println("Status command received, unimplemented")
		fmt.Println(len(settings.retrievers))
//		fmt.Println(retrieverstates)
	case ADDINSTANCE: // Spin off into a separate function
		rawhostname := make([]byte, 255)
		_, err = io.ReadFull(c, rawhostname)
		if err != nil {
			fmt.Println(err)
			c.Close()
//			continue
		}
		hostname := string(rawhostname[:])
		hostname = strings.Replace(hostname, "\x00", "", -1)

		fmt.Println("Adding instance |" + hostname + "|")
		retrieverstate := RetrieverState{}
		retrieverstate.hostname = hostname
		retrieverstate.state = ACTIVE
		retrieverstate.credentialed = false
		retrieverstate.channel = make(chan string)
		go publictimeline_retriever(hostname, post_channel, retrieverstate.channel, retrieverdata_chan)
		settings.retrievers = append(settings.retrievers, retrieverstate)
	case SUSPENDINSTANCE:
		fmt.Println("Suspend existing instance")
	case RESUMEINSTANCE:
		fmt.Println("resume existing instance")
	case SETCRAWL:
		fmt.Println(rawSendFlags[0])
		if rawSendFlags[0] == 0x00 {
			fmt.Println("Crawl toggle:", settings.crawl, "-> false")
			settings.crawl = false
		} else if rawSendFlags[0] == 0x01 {
			fmt.Println("Crawl toggle:", settings.crawl, "-> true")
			settings.crawl = true
		} else {
			fmt.Println("Protocol mismatch.")
		}
	default:
		fmt.Println("Unimplemented...")
	}

	c.Close()
}

func main() {
	//retrieverstates := make([]RetrieverState, 0)
	post_channel := make(chan Post, 10)
	retrieverdata_chan := make(chan RetrieverData, 200)
	newpeer_chan := make(chan string, 200)
	var settings Settings

	go dbwriter(post_channel)

	go peerhunter("mastodon.social", newpeer_chan)
	l, err := net.Listen("tcp", "127.0.0.1:5555")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer l.Close()

	for {

		newConns := make(chan net.Conn)

		// For every listener spawn the following routine
		go func(l net.Listener) {
			for {
				c, err := l.Accept()
				if err != nil {
				// handle error (and then for example indicate acceptor is down)
					newConns <- nil
					return
				}
				newConns <- c
			}
		}(l)

		for {
			select {
			case c := <-newConns:
				parseCtl(c, post_channel, &settings, retrieverdata_chan)
//			case retrieverdata := <-retrieverdata_chan:
//				fmt.Println(retrieverdata)
			case newpeer := <-newpeer_chan:
				fmt.Println(newpeer)
			case <-time.After(time.Minute):
				fmt.Println("Here")
				break
			}
			fmt.Println("restarting")
		}
	}
}
