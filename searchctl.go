package main

import (
	"flag"
	"fmt"
	"os"
	"net"
	"io"
)

type CommandType int8

const (
	SHUTDOWN	CommandType = 0
	STATUS		CommandType = 1
	ADDINSTANCE	CommandType = 2
	SUSPENDINSTANCE	CommandType = 3
	RESUMEINSTANCE	CommandType = 4
	SETCRAWL	CommandType = 5
)

type SendStruct struct {
	commandtype	CommandType
	flags		byte
	hostname	[255]byte
	username	[32]byte
	password	[32]byte
}

func main() {

	shutdownPtr := flag.Bool("shutdown", false, "Shutdown server")
	suspendinstancePtr := flag.String("suspend-instance", "", "Instance to Suspend")
	resumeinstancePtr := flag.String("resume-instance", "", "Instance to Resume")
	addinstancePtr := flag.String("add-instance", "", "Instance to add")
	statusPtr := flag.Bool("status", false, "Check status")
	setcrawlPtr := flag.String("setcrawl", "", "Sets a parameter")
//	usernamePtr := flag.String("username", "", "Set username")
//	passwordPtr := flag.String("password", "", "Set password")
	flag.Parse()

	var sendcommand SendStruct

	/* Condition verification */
	totalflags := 0
	sendflags := make([]byte, 1)
	sendflags[0] = uint8(0x00) // No credentials
	if *shutdownPtr == true {
		totalflags++
		sendcommand.commandtype = SHUTDOWN
	}
	if *statusPtr == true {
		totalflags++
		sendcommand.commandtype = STATUS
	}
	if *addinstancePtr != "" {
		totalflags++
		sendcommand.commandtype = ADDINSTANCE
		sendflags[0] = uint8(0x00) // No credentials
		copy(sendcommand.hostname[:], *addinstancePtr)
	}
	if *suspendinstancePtr != "" {
		totalflags++
		sendcommand.commandtype = SUSPENDINSTANCE
		copy(sendcommand.hostname[:], *suspendinstancePtr)
	}
	if *resumeinstancePtr != "" {
		totalflags++
		sendcommand.commandtype = RESUMEINSTANCE
		copy(sendcommand.hostname[:], *resumeinstancePtr)
	}
	if *setcrawlPtr != "" {
		totalflags++
		sendcommand.commandtype = SETCRAWL
		if *setcrawlPtr == "0" {
			sendflags[0] = uint8(0x00) // Enable crawling
		} else if *setcrawlPtr == "1" {
			sendflags[0] = uint8(0x01) // Enable crawling
		} else {
			fmt.Println("Invalid flag, must be 0 or 1. Exiting.")
			os.Exit(1)
		}
		fmt.Println("The value " + *setcrawlPtr)
	}
	if totalflags > 1 {
		fmt.Println("Incompatible arguments, exiting.")
		os.Exit(1)
	} else if totalflags == 0 {
		fmt.Println("No options specified, exiting.")
		os.Exit(1)
	}

	c, err := net.Dial("tcp", "127.0.0.1:5555")
	if err != nil {
		fmt.Println(err)
		return
	}

	// Send the Command
	cmd := make([]byte, 1)
	cmd[0] = uint8(sendcommand.commandtype)
	c.Write(cmd)
	c.Write(sendflags)

	if sendcommand.commandtype == ADDINSTANCE {
		c.Write(sendcommand.hostname[:])
	} else if sendcommand.commandtype == STATUS {
		sizebyte := make([]byte, 4)
		_, _ = io.ReadFull(c, sizebyte)
		i := uint32(sizebyte[0]) | uint32(sizebyte[1])<<8 | uint32(sizebyte[2]) <<16 | uint32(sizebyte[3]) << 24
		fmt.Println("Number of retrievers:", i)
	}
}
